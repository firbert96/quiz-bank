const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const questionSchema = new Schema({
    question:{
        type:'string',
        require:true
    },
    options:{
        "A":{
            type:'string',
            require:true
        },
        "B":{
            type:'string',
            require:true
        },
        "C":{
            type:'string',
            require:true
        },
        "D":{
            type:'string',
            require:true
        }
    },
    answer:{
        type:"string",
        enum:["A","B","C","D"],
        require:true
    }
});

const question = mongoose.model("Question",questionSchema);
module.exports = question;