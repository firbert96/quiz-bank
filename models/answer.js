let mongoose = require('mongoose');
const Schema = mongoose.Schema;

const answerSchema =  new Schema({
    userId:{
        type: Schema.Types.ObjectId,
        ref:'User'
    },
    score:{
        type:'number',
        required:true
    },
    answerUser:[{
        type:Schema.Types.String,
        required:true
    }],
    answerQuestion:[{
        type:Schema.Types.ObjectId,
        ref:'Question'
    }],
    result:[{
        type:Schema.Types.Boolean,
        required:true
    }]
});

const answer = mongoose.model("Answer",answerSchema);
module.exports=answer;