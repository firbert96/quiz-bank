let mongoose = require('mongoose');
const Schema = mongoose.Schema;

const quizSchema = new Schema({
    userId:{
        type: Schema.Types.ObjectId,
        ref:'User'
    },
    questionCollection:[{
        type:Schema.Types.ObjectId,
        ref:'Question'
    }]
});

const Quiz = mongoose.model("Quiz",quizSchema);

module.exports=Quiz;