const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const dbConnection = process.env.DB_CONNECTION;
mongoose.connect(dbConnection,{
    useCreateIndex:true,
    useUnifiedTopology: true,
    useNewUrlParser:true
})
.then(()=>{console.log(`Database connect to ${dbConnection}`)})
.catch(()=>{process.exit(1)});

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.get('/',(req,res)=>res.status(200)
    .send({
        status:true,
        data:'Welcome to API'
    })
)
const router = require('./router');
app.use('/api/v1',router);

module.exports=app;