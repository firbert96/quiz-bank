const express = require('express');
const router = express.Router();

const {
    createUser,
    loginUser
} = require('./controllers/userController');

const {
    createQuestion
} = require('./controllers/questionController');

const {
    createQuiz,
    viewQuiz
} = require('./controllers/quizController');

const {
    answerByUser,
    viewAnswer
} = require('./controllers/answerController');

const authentication = require('./middlewares/authentication');

//user router
router.post('/users',createUser);
router.post('/users-login',loginUser);

//question router
router.post('/questions',createQuestion);

//quiz router
router.post('/quizzes',authentication,createQuiz);
router.get('/quizzes',authentication,viewQuiz);

//answer router
router.post('/answers',authentication,answerByUser);
router.get('/answers',authentication,viewAnswer);

module.exports=router;