const jwt = require('jsonwebtoken');
const {error}=require('../helpers/response');
async function authentication (req,res,next){
    try{
        let str = req.headers.authorization;
        let token = str.split('Bearer ')[1];4
        let payload = jwt.verify(token,process.env.SECRET_KEY);
        req.headers.authorization=payload._id;
        next();
    }
    catch(err){
        return error(res,'Invalid Token',401);
    }
}

module.exports=authentication;