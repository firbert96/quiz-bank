const Quiz = require('../models/quiz');
const Question = require('../models/question');
const {success, error}=require('../helpers/response');

async function createQuiz (req,res){
    let findQuizByUserId= await Quiz.findOne({userId:req.headers.authorization});
    if(!findQuizByUserId){
        let findAllQuestion = await Question.find();
        let randomQuestion=[];
        let randomNumber=0;
        for (let i =0;i<10;i++){
            randomNumber=Math.floor(Math.random() * Math.floor(findAllQuestion.length));
            randomQuestion.push(findAllQuestion[randomNumber]._id);         
        }
        let quiz = {
            userId:req.headers.authorization,
            questionCollection:randomQuestion
        }
        try{
            let saveQuiz=await Quiz.create(quiz);
            return success(res,saveQuiz,201);
        }
        catch(ex){
            return error(res,ex,422);
        }
    }
    else{
        return success(res,'Quiz already created',200);
    }
}

function viewQuiz(req,res){
    Quiz.findOne({userId:req.headers.authorization})
    .select([
        'userId',
        'questionCollection'
    ])
    .populate([
        {
            path:'userId',
            select:['name','email']
        },
        {
            path:'questionCollection',
            select:['question','options']
        }
    ])
    .then(i=>{
        return success(res,i,200);
    })
    .catch(err=>{
        return error(res,err,422);
    });
}

module.exports={
    createQuiz,
    viewQuiz
};