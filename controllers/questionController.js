const Question = require('../models/question');
const {success, error} = require('../helpers/response');

function createQuestion(req,res){
    const{question,options,answer} = req.body;
    let modelQuestion = new Question({
        question:question,
        options:options,
        answer:answer
    });
    modelQuestion.save()
    .then(i=>{
        return success(res,i,201);
    })
    .catch(err=>{
        return error(res,err,422);
    })
}

module.exports={
    createQuestion
}