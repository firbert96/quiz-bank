const Answer = require('../models/answer');
const Question = require('../models/question');
const Quiz = require('../models/quiz');
const {success, error}=require('../helpers/response');

async function answerByUser (req,res){
    const userId = req.headers.authorization;
    let findQuizByUserId= await Quiz.findOne({userId:userId});
    if(findQuizByUserId){
        let answerQuestion=findQuizByUserId.questionCollection;
        let answerUser = req.body.answer;
        let result=[];
        let populateAnswer =[];
        let score =0;
        for (let i in answerQuestion){
            try{
                let findAnswer=await Question.findById(answerQuestion[i]);
                populateAnswer.push(findAnswer.answer);
            }
            catch(ex){
                return error(res,ex,422);
            }
        }
        for (let i=0;i<answerQuestion.length;i++){
            if(answerUser[i]===populateAnswer[i]){
                result.push(true);
                score++;
            }
            else{
                result.push(false);
            }
        }
        let answer = new Answer({
            userId:userId,
            score:score,
            answerQuestion:answerQuestion,
            answerUser:answerUser,
            result:result
        });
        try{
            let saveAnswer=await Answer.create(answer);
            return success(res,saveAnswer,201);
        }
        catch(ex){
            return error(res,ex,422);
        }
    }
    else{
        return error(res,'Please create question');
    }
}

function viewAnswer (req,res){
    Answer.findOne({userId:req.headers.authorization})
    .select([
        'userId',
        'score',
        'answerUser',
        'answerQuestion',
        'result'
    ])
    .populate([
        {
            path:'userId',
            select:['name','email']
        },
        {
            path:'answerQuestion',
            select:['answer']
        }
    ])
    .then(i=>{
        return success(res,i,200);
    })
    .catch(err=>{
        return error(res,err,422);
    });

}

module.exports={
    answerByUser,
    viewAnswer
}