const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {success, error}=require('../helpers/response');

async function loginUser (req,res){
    const {email,password}=req.body;
    let login = await User.findOne({email});

    // validator email
    if(!login){
        return error(res,'Email not found',404);
    }
    
    let result = bcrypt.compareSync(password,login.password);
    
    if(result){
        var token = jwt.sign({ _id: login._id }, process.env.SECRET_KEY);
        return success(res,token,200);
    }
    else{
        return error(res,'Password isn\'t match',406);
    }
}

function createUser (req,res){
    const {name,email,password} = req.body;
    let salt = bcrypt.genSaltSync(10);
    let passwordDigest = bcrypt.hashSync(password,salt);
    let user = new User({
        name:name,
        email:email,
        password:passwordDigest
    })
    user.save()
    .then(i=>{
        return success(res,i,201);
    })
    .catch(err=>{
        return error(res,err,422);
    })
}

module.exports={
    createUser,
    loginUser
}